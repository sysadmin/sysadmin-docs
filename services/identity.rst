KDE Identity
============

KDE Identity is the single sign-on system for KDE websites.
It consists of an LDAP server
and a custom web application
for signup and account management.
KDE developers and users only need to register once at KDE Identity,
and they can use that account to login to most KDE websites.

To sign up, users enter their email address,
and the website sends a confirmation link.
After clicking the confirmation link,
they enter a username and password.
Login is then done with the username.
Usernames are unique, and cannot be changed by users
(sysadmins can change a username,
but currently it takes lots of manual work to do it).

To login to an Identity-enabled website,
users simply enter their KDE Identity
username and password on the website itself,
which verifies the credentials via LDAP.

Identity is currently hosted on :doc:`/servers/orbi`.

There are Ansible playbooks to do all the setup of
Apache, MySQL, PHP, LDAP, and the website code.
The playbooks are complete enough to replicate the environment locally.

Local testing
-------------

To easily install a local clone of Identity,
to work on either the deployment or the web code::

    git clone https://invent.kde.org/sysadmin/kde-ansible
    git clone https://invent.kde.org/websites/identity-kde-org
    cd identity-kde-org
    vagrant up

You can now access the Identity website at localhost:8080.
All emails the website sends (such as confirmation links when creating an account)
will be captured by Mailhog; you can go to localhost:8025 to read them.

The initial admin account is 'site-admin',
with password '(replaceme)' including the parentheses.
You can login to the website with it.

To access the ldap server from the host machine:

.. code-block::

    ldapsearch -H ldap://localhost:50389 -x -D uid=site-admin,ou=people,dc=kde,dc=org -w '(replaceme)' -LLL -b "dc=kde,dc=org" "uid=site-admin"



Services using Identity
-----------------------

.. todo::
  Several Wordpress and Drupal instances use Identity too;
  need to track those down.

The websites and services that are currently integrated with KDE Identity login are:

- `GitLab <gitlab>`_
- Phabricator
- Nextcloud
- Forum (phpBB)
- MediaWiki (via Phabricator OAuth)
- Jenkins (via Phabricator OAuth)
- Events platform (events.kde.org)
- Conference management (conf.kde.org)
- Travel reimbursements
- Akademy volunteer management
- BigBlueButton
- Website statistics (stats.kde.org)
- Mailserver (sending email through mail.kde.org)
- `Grafana <monitoring>`_
- Ballot (KDE eV voting)

- Wordpress

  - Labplot
  - Kdenlive
  - Krita
  - Neon blog

- Drupal

  - Akademy website


Notable services that *don't* use Identity (have independent accounts):

- Bugzilla
- LimeSurvey
- ZNC
- dot.kde.org
- blogs.kde.org

LDAP
----

The server runs OpenLDAP.
Debian/Ubuntu by default installs OpenLDAP
with settings stored in a ``/etc/ldap/slapd.d`` directory
in ldif format, where changes are made through ldapadd/ldapmodify.
We disabled this and changed it back to
a plain ``/etc/ldap/slapd.conf`` file.

The LDAP daemon listens only on localhost;
it's not accessible over the Internet.
Servers running Identity-enabled websites
connect to LDAP via an SSH tunnel.

There is a `custom KDE LDAP schema <https://commits.kde.org/websites/identity-kde-org?path=protected/data/kde.schema>`_,
adding the object classes ``kdeAccount`` and ``kdeEvMember``
and their attributes.
They are under the registered OID ``1.3.6.1.4.1.36417``.

Users are stored with LDAP object class ``inetOrgPerson`` and ``kdeAccount``.
Members of KDE eV additionally have the ``kdeEvMember`` object class,
which adds ``memberStatus`` and ``evMail`` attributes.

Groups have both ``posixGroup`` and ``groupOfNames`` object classes.
Groups list all their members as usernames in ``memberUid`` attributes (``posixGroup``)
and as DNs in ``member`` attributes (``groupOfNames``).
Users also have a ``groupMember`` attribute
pointing back at the group name.

A few groups grant additional permissions within LDAP,
via ACLs in slapd.conf:

``users``
  *All* users belong to this group
``sysadmins``
  Active members of the KDE sysadmin team.
  They get permissions to modify *everything*.
``developers``
  KDE developers with commit access.
  Developers can have SSH public keys in their profile.
  Their basic info (username, name, email) can be seen by everyone.
  They can see detailed info about other developers.
  Group membership here is synchronized into access to kde/* in GitLab.
``disabled-developers``
  Developers who had their commit access disabled
  (usually on their request).
  They can still have SSH public keys, but they have no effect.
  They get no permissions on GitLab.
  Everything else is the same as ``developers`` (eg. public email addresses).
``ev-members``
  Members of KDE e.V (including inactive).
  They can see information about other eV members.
  They have memberStatus and evMail attributes,
  and they can change their own evMail.
``ev-board``
  Board members of KDE e.V.
  Board members can edit the ``ev-members`` and ``ev-board`` groups,
  can see and change every ev-members' evMail and memberStatus,
  and can see basic info about every user
  (so that they can add any user to ev-members).
``identity-support``
  People who can give Identity tech support.
  They can see info about every user,
  in order to deal with user requests about forgetting username, etc.

All other groups have no effect on LDAP attribute permissions
or other behavior of Identity itself.
They are only used by other services
to manage their own permissions.
For example, access to website statistics
or to some Nextcloud shared folders
is controlled via these groups.

.. todo::
  SSH keys in LDAP aren't used anymore;
  developers upload their keys to GitLab.
  Above docs need fixing to reflect that.
  Perhaps the Neon git repositories do still use them;
  need to investigate.

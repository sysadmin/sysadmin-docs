GitLab
======

Sysadmin team hosts
`GitLab Core edition <https://about.gitlab.com/install/ce-or-ee/>`__
for the source code hosting.

GitLab is currently hosted on :doc:`leptone </servers/leptone>`.

Installation
------------

We have opted to use
`source based installation method <https://docs.gitlab.com/ee/install/installation.html>`__
for installing the GitLab.

Supporting infrastructure
-------------------------

In addition to GitLab
we also host some other components,
Which include:

- `Gitlab LDAP Sync <https://invent.kde.org/sysadmin/gitlab-ldapsync>`__
- `Replicant <https://invent.kde.org/sysadmin/replicant>`__
- `Gitlab React <https://invent.kde.org/sysadmin/gitlab-react>`__

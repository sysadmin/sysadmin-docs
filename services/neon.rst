KDE Neon
========

This documents various infrastructure for the `KDE
Neon <https://neon.kde.org>`__ project.

Main parts of infrastructure hosted by KDE:

-  Jenkins master at `build.neon.kde.org <https://build.neon.kde.org>`__
-  Hosting for the KDE Neon ISO and images at
   `files.kde.org/neon <https://files.kde.org/neon>`__
-  Aptly package archive at
   `archive.neon.kde.org <https://archive.neon.kde.org>`__

Jenkins master
--------------

Jenkins master for KDE Neon is hosted on
:doc:`charlotte </servers/charlotte>`. Jenkins master runs as the
``neon`` user on port ``8400``. It's reverse-proxied by Apache, on
virtualhost build.neon.kde.org.

Jenkins data is stored on the /home/neon/data.

Currently following people have access to the ``neon@charlotte``  [1]_

-  Harald Sitter
-  Jonathan Riddell
-  Bhushan Shah  [2]_
-  Scarlett Moore

Aptly archive
-------------

Todo
----

-  document more

.. [1]
   Currently there's also ``jenkins@do-neon-jenkins`` access key in
   ``authorized_keys`` which is used by Jenkins master to talk to
   slaves, but that being in ``neon`` user's ``authorized_keys`` does
   not make much sense, todo: investigate more

.. [2]
   Since Bhushan have access to ``root@charlotte`` he does not have
   access key added to the ``neon`` user, should this change in future,
   his key should be added

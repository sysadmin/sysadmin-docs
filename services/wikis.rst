MediaWiki
=========

How to update
-------------

1. Read `upgrading manual <https://www.mediawiki.org/wiki/Manual:Upgrading>`__
2. ``cd /srv/www/mediawiki/``
3. ``wget https://releases.wikimedia.org/mediawiki/<major_version>/mediawiki-<version>.tar.gz``
4. ``tar -xvzf mediawiki-<version>.tar.gz``
5. ``rm mediawiki-<version>.tar.gz``
6. Copy LocalSettings.*, TranslationSettings.*, images* and .htaccess from the old mediawiki install to the new one
7. Copy and update ``extensions/install_extension.sh`` from the old install to the new one
8. Do database backup from the old install
9. ``WIKI=wikisandbox.kde.org php maintainance/update.php``. This often requires manual sql intervention.
10. ``WIKI=wikisandbox.kde.org php maintenance/rebuildLocalisationCache.php``
11. ``WIKI=wikisandbox.kde.org php maintenance/runJobs.php``
12. Go to ``/srv/www/mediawiki/sites`` and do ``ln -sfn ../mediawiki/ wikisandbox.kde.org``
13. Make sure everything works (most important: authentification and translations since it always breaks)
14. Repeat from step 8 with the other wikis
15. Update `job queue scripts <https://www.mediawiki.org/wiki/Manual:Job_queue>`__ with new path

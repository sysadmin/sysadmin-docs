Build nodes
===========

:doc:`eucten </servers/eucten>` and :doc:`arkyid </servers/arkyid>` are set up the same way,
so the common information is explained here.

They run Docker (as Docker Swarm node) for Linux builds,
and QEMU-kvm virtual machines for Windows and FreeBSD.

Storage
-------

The NVMe SSDs partition layout is as follows:

============ ===== ==========================
Partition    Size  Use
============ ===== ==========================
nvme[01]n1p1 512MB raid1, /boot
nvme[01]n1p2 25GB  raid1, /
nvme[01]n2p2 928GB raid1, LVM physical volume
============ ===== ==========================

The LVM volume group is then divided as follows:

====================== ===== ==============================
Volume                 Size  Use
====================== ===== ==============================
linux-data             500GB /mnt/data
WindowsBuilder\ *Name* 300GB raw disk device for Windows VM
FreeBSDBuilder\ *Name* 100GB raw disk device for FreeBSD VM
====================== ===== ==============================

(The virtual machine name differs between servers)

Also, ``/mnt/data/home/`` is bind-mounted to ``/home/``

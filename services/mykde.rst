MyKDE
=====

MyKDE is the next generation replacement for :doc:`/services/identity`.
It is an OAuth2 provider that provide KDE specifics features.

Deployment
----------

After cloning the Git repo, perform these steps to create a working dev server:

- Copy ``mykdeorg/__settings.py`` to ``mykdeorg/settings.py`` and edit the database configuration.
- Create a virtual environment (with Python 3.6) by running ``poetry install``
  (if you get an error when installing the ``mysqlclient`` package on macOS,
  `change your mysql_config <https://github.com/PyMySQL/mysqlclient-python#note-about-bug-of-mysql-connectorc-on-macos>`_).
  Note that from now on we assume you run from a ``poetry shell`` or prefix commands with
  ``poetry run``.
- Create the database with ``mykdeorg``.
- Run ``./manage.py migrate`` to migrate your database to the latest version.
- Run ``./manage.py createcachetable`` to create the cache table in the database.
- Run ``mkdir media`` to create the directory that'll hold uploaded files
  (such as images for the badges).
- In production, set up a cron job that calls the
  `cleartokens <https://django-oauth-toolkit.readthedocs.io/en/latest/management_commands.html#cleartokens>`_
  management command regularly.
- In production, set up a cron job that calls the ``flush_webhooks --flush -v 0`` management command
  regularly.
- Run ``./manage.py createsuperuser`` to create super user
- Load any fixtures you want to use.
   - list fixtures  ``ls */fixtures/*``
   - ``./manage.py loaddata default_site``
   - ``./manage.py loaddata flatpages``
- Run ``./manage.py collectmedia`` to collect media from fixtures and place into the media directory.
- Run ``./manage.py runserver``

In production, you will instead need to use the following apache configuration and
install the mod_wsgi apache plugin.

Example Apache configuration
----------------------------

.. code-block::

    <VirtualHost *:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        #ServerName www.example.com

        ServerAdmin webmaster@localhost

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
        Redirect / https://gallien.kde.org
    </VirtualHost>

    <VirtualHost *:443>
        ServerAdmin webmaster@kde.org
        DocumentRoot /var/www/empty

        <Directory /srv/www/my-kde-org/mykdeorg>
            <Files wsgi.py>
                Require all granted
            </Files>
        </Directory>

        Alias /robots.txt /srv/www/my-kde-org/static/robots.txt
        Alias /favicon.ico /srv/www/my-kde-org/static/favicon.ico

        Alias /media/ /srv/www/my-kde-org/media/
        Alias /static/ /srv/www/my-kde-org/static/

        <Directory /srv/www/my-kde-org/media>
            Require all granted
        </Directory>

        <Directory /srv/www/my-kde-org/static>
            Require all granted
        </Directory>
        WSGIDaemonProcess mykdeorg python-home=/srv/www/my-kde-org/.venv python-path=/srv/www/my-kde-org
        WSGIProcessGroup mykdeorg
        WSGIScriptAlias / /srv/www/my-kde-org/mykdeorg/wsgi.py
        WSGIPassAuthorization On

        SetEnv DEBUG True # change to False in production
        SetEnv DEFAULT_FROM_EMAIL "noreply@kde.org"
        SetEnv SECRET_KEY "my-secret-key"
        SetEnv DATABASE_URL "mysql://username:password://127.0.0.1:3306/mykdeorg"

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # Enable SSL
        Include /etc/apache2/common/ssl.conf
        Include /etc/apache2/common/ssl-star-kde-org.conf                                                                                                                                           
    </VirtualHost>

    # vim: syntax=apache ts=4 sw=4 sts=4 sr noet

Setting up a MyKDE connected service:
------------------------------------

Path to user information: ``$DOMAIN/api/me``

Path to authorize url: ``$DOMAIN/oauth/authorize``

Path to token endpoint: ``$DOMAIN/oauth/token``

You can add a new application in https://my.kde.org/admin/bid_main/oauth2application/

Setting up the Season of KDE (and other ``bid_api`` users)
----------------------------------------------------------

To allow the Season of KDE to call our API endpoints, do the following:

- Add an OAuth2 application for the API, name it "MyKDE API". You can choose any name, but
  it's nice if we all use the same so we can recognise it. Set it to 'Confidential' and 'Resource
  owner password-based'.
- Make sure that the ``sok-student`` and ``sok-student`` roles exist, and that they are badges.
  Add a role ``season-admins`` and allow it to manage the above roles.
- Add a user for the Season of KDE website, for example using "yourname+season@yourdomain.com" as email
  address. The password doesn't matter -- give it a long one and forget about it. Give the user the
  ``season-admins`` role.
- Add an OAuth2 token to the Season of KDE website user for the MyKDE API application. Make sure it
  doesn't expire any time soon (e.g. somewhere in the year 2999). Give it the scopes
  ``badger``.
- Configure the Season of KDE website to use this token to authenticate its API calls.

code
====

LXC container running in :doc:`ctenzi`.

Services
--------

- Phabricator
- Gitolite-based git hosting for KDE Neon

Backups
-------

Backups are saved into the shared storage box on Hetzner.
Some things are still stored here and still being backed up,
but the canonical version has been moved to another server
(for example the eV board SVN repository).
It's pending cleanup.

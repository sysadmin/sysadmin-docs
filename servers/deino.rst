deino
=====

LXC container running in :doc:`hersili`.

Services
--------

TODO

Name etymology
--------------

   The `Deinopoidea <https://en.wikipedia.org/wiki/Deinopoidea>`__
   or deinopoids are group of cribellate araneomorph spiders that may be treated as a superfamily.
   The group is characterized by the production of orb webs with catching threads of cribellate silk.

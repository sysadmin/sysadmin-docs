overwatch
=========

This is a virtual machine donated by Gaertner.

Specifications
--------------

- 2 cores
- 4GB RAM
- 50GB disk
- Ubuntu 16.04 (xenial)

The IPv4 address is 217.13.79.76. IPv6 is not set up.

Services
--------

This server is in charge of monitoring the rest of our infrastructure.
It runs `InfluxDB <https://docs.influxdata.com/influxdb/>`__ to store recorded metrics
and `Grafana <https://grafana.com/>`__ to visualize them and send alerts.
Every server (including overwatch itself) runs `Telegraf <https://docs.influxdata.com/telegraf/>`__
to gather metrics and send them to InfluxDB.

More information about the setup is in the :doc:`Infrastructure monitoring </services/monitoring>` page.

Backups
-------

Overwatch backups are saved in :doc:`micrea`.

- rsync (retention: 7 daily)

  - /etc
  - user crontabs
  - dpkg package lists
  - Grafana SQLite database file

- Borg (retention: 7 daily, 4 weekly, 6 monthly)

  - InfluxDB database

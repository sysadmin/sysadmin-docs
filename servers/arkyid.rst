arkyid
======

Physical server hosted at Hetzner, model
`AX51-NVMe <https://www.hetzner.com/dedicated-rootserver/ax51-nvme>`__.
It's identical to :doc:`eucten`.

Specifications
--------------

- 64GB DDR4 RAM
- 8-core AMD Ryzen 7 3700X
- 2x 1TB SSD
- Ubuntu 18.04 (bionic)

Services
--------

This server is a :doc:`Jenkins build node </services/build-node>`.
It runs Docker for Linux builds,
and QEMU-kvm virtual machines for Windows and FreeBSD.
The VMs are named "BuilderIota".

Backups
-------

None; this server doesn't hold persistent data.

Name etymology
--------------

   `Arkyidae <https://en.wikipedia.org/wiki/Arkyidae>`__ is a family of
   araneomorph spiders first described by Ludwig Carl Christian Koch in
   1872 as a subfamily of Araneidae, and later elevated to a full family
   in 2017.

eucten
======

Physical server hosted at Hetzner, model
`AX51-NVMe <https://www.hetzner.com/dedicated-rootserver/ax51-nvme>`__.
It's identical to :doc:`arkyid`.

Specifications
--------------

- 64GB DDR4 RAM
- 8-core AMD Ryzen 7 3700X
- 2x 1TB SSD
- Ubuntu 18.04 (bionic)

Services
--------

This server is a :doc:`Jenkins build node </services/build-node>`.
It runs Docker for Linux builds,
and QEMU-kvm virtual machines for Windows and FreeBSD.
The VMs are named "BuilderKappa".

Backups
-------

None; this server doesn't hold persistent data.

Name etymology
--------------

   The `Euctenizidae <https://en.wikipedia.org/wiki/Euctenizidae>`__
   (formerly Cyrtaucheniidae subfamily Euctenizinae) are a family of
   mygalomorph spiders.

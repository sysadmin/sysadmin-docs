ctenzi
======

Physical server hosted at Hetzner.
Apparently this is model
`EX42 <https://www.hetzner.com/dedicated-rootserver/ex42>`__
with additional 2x512GB SSDs.

Specifications
--------------

- 64GB DDR4 RAM
- Quad-core Intel i7-6700
- 2x 4TB HDD
- 2x 512GB SSD
- Ubuntu 18.04 (bionic)

Containers
----------

LXC containers in this server:

- :doc:`leptone`: hosts GitLab and SVN
- :doc:`gallien`: MyKDE test environment
- :doc:`code`: hosts Phabricator and KDE Neon repositories

Storage
-------

The NVMe SSDs partition layout is as follows:

============ ===== ============
Partition    Size  Use
============ ===== ============
nvme[01]n1p1 512MB raid1, /boot
nvme[01]n1p2 476GB raid1, /
============ ===== ============

The hard disks have only one partition:

========= ===== ================
Partition Size  Use
========= ===== ================
sd[ab]1   3.7TB raid1, /mnt/data
========= ===== ================

This filesystem stores the SVN and Git repository data used by leptone,
and the entire code container.

Backups
-------

There is nothing interesting in ctenzi itself,
the important data is in the containers,
so ctenzi has no backups.
Backups are done by each container.

Name etymology
--------------

   `Ctenizidae <https://en.wikipedia.org/wiki/Ctenizidae>`__ is a small
   family of medium-sized mygalomorph spiders that construct burrows
   with a cork-like trapdoor made of soil, vegetation and silk. They may
   be called trapdoor spiders.

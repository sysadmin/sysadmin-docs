hersili
=======

Physical server hosted at Hetzner.
Model `AX41 <https://www.hetzner.com/dedicated-rootserver/ax51>`__
with additional 2 x 1 TB NVMe SSD.

Specifications
--------------

- 64GB DDR4 RAM
- AMD Ryzen 5 3600 Hexa-Core
- 2x 8TB HDD
- 2x 1TB SSD
- Ubuntu 20.04 (focal)

Containers
----------

LXC containers in this server:

- :doc:`deino`
- :doc:`seges`
- :doc:`telemid`

Storage
-------

The NVMe SSDs partition layout is as follows:

============ ====== =============
Partition    Size   Use
============ ====== =============
nvme[01]n1p1 8GB    raid1, swap
nvme[01]n1p2 512MB  raid1, /boot
nvme[01]n1p2 945GB  raid1, /
============ ====== =============

The hard disks have only one partition:

========= ===== ==========================
Partition Size  Use
========= ===== ==========================
sd[ab]1   2TB   raid1, LVM physical volume
========= ===== ==========================

The LVM volume group is then divided as follows:

============= ====== ===================================
Volume        Size   Use
============= ====== ===================================
vg0-backups   2TB    backup data from other servers
vg0-gitlab    500GB  backups of GitLab repositories
vg0-maps      500GB  maps tiles, and data for distribute.k.o and cdn.k.o
vg0-archives  4TB    download.k.o and files.k.o data
============= ====== ===================================

There is still ~450GB of unallocated space in the VG.

Backups
-------

There is nothing interesting in hersili itself,
the important data is in the containers,
so hersili has no backups.
Backups will be done by each container.

Name etymology
--------------

   `Hersiliidae <https://en.wikipedia.org/wiki/Tree_trunk_spider>`__
   is a tropical and subtropical family of spiders
   first described by Tamerlan Thorell in 1870,
   which are commonly known as tree trunk spiders.

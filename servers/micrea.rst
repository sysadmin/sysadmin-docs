micrea
======

Physical server hosted at Hetzner, model `EX41 <http://web.archive.org/web/20180817205112/https://www.hetzner.com/dedicated-rootserver/ex41>`__.

Specifications
--------------

- 32GB DDR4 RAM
- Quad-core Intel i7-6700
- 2x 4TB HDD
- Ubuntu 18.04 (bionic)

Containers
----------

LXC containers in this server:

- :doc:`milonia`
- :doc:`mimi`

(capona used to be here but it was moved to anepsion;
you may still see it in the assigned IP addresses
and in ``lxc-ls``)

Storage
-------

Both disks have identical layout:

========= ============ ========================
Partition Size         Use
========= ============ ========================
sd[ab]1   2GB          raid1, /boot
sd[ab]2   50GB         raid1, /
sd[ab]3   1MB          ?
sd[ab]4   3.6TB (rest) 'tank' zfs pool (mirror)
========= ============ ========================

The zfs pool is then used for the following filesystems,
which share disk space (there isn't a size limit for each):

- containers/capona
- containers/milonia
- containers/mimi
- data/backups
- data/cdn
- data/distribute
- data/download
- data/files
- data/git-repositories
- data/maps

Backups
-------

Micrea doesn't have backups of its own,
most things are backed up by the corresponding container.

Micrea is a backup *destination* for several servers.
For each server that does backups into micrea,
there is a user account ``${hostname}backup``
with home directory ``/home/BACKUP-${hostname}.kde.org``.
(Note that /home is a symlink to /tank/data/backups
so this is stored in the zfs pool).
Servers can use rsync and/or borg to do their backups here.

Most of this setup is handled by ansible playbooks.

letterbox
=========

Cloud server hosted at :doc:`Bytemark </providers/bytemark>`.

Specifications
--------------

- 12GB RAM
- 4 cores
- 55GB SSD
- 200GB HDD
- Ubuntu 18.04 (bionic)

Services
--------

This is the main mail server.

- postfix
- mailman
- antispam stuff

(TODO: this needs a ton more documentation)

Backups
-------

Backups are sent to :doc:`micrea`.

Rsync:

- /etc, cronjobs, package lists
- mailman install

Borg (retention: 7 daily, 4 weekly, 6 monthly):

- mailman data (including the giant mailing list archives)

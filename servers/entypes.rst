entypes
=======

Virtual machine provided by `Fosshost <https://fosshost.org/>`__.

Specifications
--------------

- 6 vCPUs
- 8 GB RAM
- 400GB disk
- Ubuntu 20.04 (Focal)

Services
--------

- Telemetry (KUserFeedback) and its Grafana dashboards
- Website stats (Matomo)

telemid
=======

LXC container running in :doc:`hersili`.

Services
--------

TODO

Name etymology
--------------

   `Telemidae <https://en.wikipedia.org/wiki/Telemidae>`__,
   also known as long-legged cave spiders, is a family of small haplogyne spiders.
   Most are cave dwelling spiders with six eyes, though some do not have any eyes at all.

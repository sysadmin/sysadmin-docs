seges
=====

LXC container running in :doc:`hersili`.

This server is only used internally,
so it's only accessible over IPv6
to avoid having to purchase an extra IPv4 address.
If you don't have IPv6 access,
you can tunnel through another server (like hersili)
to connect to SSH.

You can add the following to your local ``~/.ssh/config``::

    Host seges.kde.org
        ProxyJump root@hersili.kde.org

Services
--------

This server will keep real-time backups of GitLab repositories.

Name etymology
--------------

   `Tube-dwelling spiders (Segestriidae) <https://en.wikipedia.org/wiki/Tube-dwelling_spider>`__
   is a family of araneomorph spiders first described by Eugène Simon in 1893.
   Members of this family are easily recognized
   because their first three pairs of legs are arranged forward instead of two
   and they have six eyes instead of eight, arranged in a semicircle.

gallien
=======

LXC container running in :doc:`ctenzi`.

Runs Ubuntu 20.04 (focal).

Services
--------

- :doc:`/services/mykde`

Backups
-------

Backups are saved into the shared storage box on Hetzner.
The ``/srv/`` directory and the mykde MySQL dump
are backed up using Borg.

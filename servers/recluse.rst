recluse
=======

Physical server hosted at Hetzner, model
`EX51-SSD <http://web.archive.org/web/20180816161229/https://www.hetzner.com/dedicated-rootserver/ex51-ssd>`__.

**This server was decommissioned on 2020-09-13.**

Specifications
--------------

- 64GB DDR4 RAM
- Quad-core Intel i7-6700
- 2x 500 GB SSD
- Ubuntu 16.04 (xenial)

Containers
----------

The only LXC container on recluse was :doc:`code`,
which hosts Phabricator.
:doc:`code` has since been moved to :doc:`ctenzi`.

Storage
-------

Both disks have identical layout:

========= ============ ========================
Partition Size         Use
========= ============ ========================
sd[ab]1   2GB          empty?
sd[ab]2   1GB          /boot in md raid1
sd[ab]3   20GB         / in md raid10
sd[ab]4   442GB (rest) 'tank' zfs pool (mirror)
========= ============ ========================

The zfs pool is then used for the following filesystems,
which share disk space (there isn't a size limit for each):

- containers/code
- data/evboard
- data/git
- data/neon
- data/phabricator
- data/staging
- data/sysadmin

Backups
-------

There is nothing interesting in recluse itself;
the important data is in the 'code' container.
Thus, the backup script only gets configuration info:
``/etc``, user cronjobs, and list of installed packages.
They are sent to the Hetzner backup space via sftp.

Name etymology
--------------

   The `recluse
   spiders <https://en.wikipedia.org/wiki/Recluse_spider>`__, also known
   as brown spiders, fiddle-backs, violin spiders, and reapers, is a
   genus of spiders that was first described by R. T. Lowe in 1832. They
   are venomous spiders known for their bite, which sometimes produces a
   characteristic set of symptoms known as loxoscelism.

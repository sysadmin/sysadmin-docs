orbi
====

Cloud server hosted at :doc:`Bytemark </providers/bytemark>`.

Specifications
--------------

-  1GB RAM
-  1 core
-  25GB SSD
-  Ubuntu 16.04 (xenial)

Services
--------

This server runs the LDAP server and the website
for KDE Identity.
See the :doc:`KDE Identity </services/identity>` page
for details.

The LDAP server listens only on localhost.
Other servers needing access to LDAP
use a ssh tunnel via the ``ldapconnect`` user.

Backups
-------

Backups are sent to :doc:`micrea`.

- /srv
- /home
- mysqldump of all databases
- LDAP contents via slapcat

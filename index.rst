.. sysadmin-docs documentation master file, created by
   sphinx-quickstart on Tue Jun  2 08:35:29 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sysadmin team's documentation!
=========================================

.. toctree::
   :maxdepth: 1
   :caption: Services
   :name: sec-services

   services/backups
   services/build-node
   services/gitlab
   services/mykde
   services/identity
   services/mirrorbrain
   services/monitoring
   services/neon

.. toctree::
   :maxdepth: 1
   :caption: Providers
   :name: sec-providers

   providers/hetzner
   providers/bytemark
   providers/digitalocean

.. toctree::
   :maxdepth: 1
   :caption: Servers
   :name: sec-servers

   servers/anepsion
   servers/arkyid
   servers/charlotte
   servers/code
   servers/ctenzi
   servers/deino
   servers/entypes
   servers/eucten
   servers/gallien
   servers/hersili
   servers/letterbox
   servers/micrea
   servers/nephilia
   servers/orbi
   servers/overwatch
   servers/recluse
   servers/seges
   servers/telemid

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

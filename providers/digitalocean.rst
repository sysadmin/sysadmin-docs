DigitalOcean
============

`DigitalOcean <https://www.digitalocean.com>`__ is cloud service provider
which sponsors DigitalOcean credits under
`DigitalOcean opensource program <https://www.digitalocean.com/open-source/>`__

Current servers:

- :doc:`ereo </servers/ereo>`
- :doc:`relate-dev </servers/relate-dev>`
- :doc:`platna </servers/platna>`
- :doc:`oxyopi </servers/oxyopi>`

Spaces
------

We also have S3 compatible object storage
at DigitalOcean

- :doc:`kde-nextcloud-storage </services/nextcloud>`

For hardware reset, tech support, etc.
use the control panel at
https://cloud.digitalocean.com/

We have a KDE Sysadmin account for DigitalOcean
for which credentials can be found in LastPass,
however sysadmin team is part of KDE project
which allows to perform most actions
right from their personal accounts

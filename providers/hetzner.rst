Hetzner
=======

Many KDE servers are physical dedicated servers
rented from `Hetzner <https://www.hetzner.com/>`__.
No donation or sponsoring is involved;
they are paid by the KDE eV.

Current servers:

- :doc:`anepsion </servers/anepsion>`
- :doc:`micrea </servers/micrea>`
- :doc:`nephilia </servers/nephilia>`
- :doc:`ctenzi </servers/ctenzi>`
- :doc:`eucten </servers/eucten>`
- :doc:`arkyid </servers/arkyid>`
- :doc:`hersili </servers/hersili>`

For hardware reset, tech support, etc.
use the control panel at
https://robot.your-server.de/

Note that :doc:`ange </servers/ange>` is also hosted at Hetzner,
but it's a sponsored machine and not part of our Hetzner account.

Storage boxes
-------------

With each server, Hetzner also provides
a free 100GB "storage box", which we use for backups.
They support sftp, rsync, and borgbackup.
They aren't restricted to the associated server,
so sometimes we may make a server store its backups
into the storage box of another server,
for disk space reasons.

We also have a separate 2TB storage box, used for other backups
that wouldn't otherwise fit in the free 100GB ones.

The credentials for storage boxes are available (encrypted)
in ``backups-vault.yml`` in the Ansible playbook repository.
